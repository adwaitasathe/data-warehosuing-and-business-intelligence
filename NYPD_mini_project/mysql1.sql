--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      NYPD_ALL.DM1
--
-- Date Created : Wednesday, April 01, 2020 20:46:26
-- Target DBMS : MySQL 5.x
--

-- 
-- TABLE: Dim_Age_Group 
--

CREATE TABLE Dim_Age_Group(
    Age_SK       INT         AUTO_INCREMENT,
    AGE_GROUP    CHAR(50),
    PRIMARY KEY (Age_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: Dim_Borough 
--

CREATE TABLE Dim_Borough(
    Boro_SK    INT         AUTO_INCREMENT,
    BORO       CHAR(50),
    PRIMARY KEY (Boro_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: Dim_Jurisdiction 
--

CREATE TABLE Dim_Jurisdiction(
    Jurisdiction_SK      INT         AUTO_INCREMENT,
    JURISDICTION_CODE    INT,
    JURIS_DESC           CHAR(50),
    PRIMARY KEY (Jurisdiction_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: Dim_KY_Code 
--

CREATE TABLE Dim_KY_Code(
    KY_SK         INT         AUTO_INCREMENT,
    KY_CD         INT,
    OFNS_DESC     CHAR(50),
    LAW_CAT_CD    INT,
    PRIMARY KEY (KY_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: Dim_LawSection 
--

CREATE TABLE Dim_LawSection(
    LAWSECTION_SK         INT         AUTO_INCREMENT,
    LAW_SECTION_NUMBER    INT,
    LAW_DESCRIPTION       CHAR(50),
    PRIMARY KEY (LAWSECTION_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: Dim_PD_Code 
--

CREATE TABLE Dim_PD_Code(
    PD_SK      INT         AUTO_INCREMENT,
    PD_CD      INT,
    PD_DESC    CHAR(50),
    PRIMARY KEY (PD_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: Dim_Race 
--

CREATE TABLE Dim_Race(
    Race_SK     INT         AUTO_INCREMENT,
    VIC_RACE    CHAR(50),
    PRIMARY KEY (Race_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: Dim_Sex 
--

CREATE TABLE Dim_Sex(
    Sex_SK      INT         AUTO_INCREMENT,
    SUSP_SEX    CHAR(50),
    PRIMARY KEY (Sex_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: Fact_Complaints 
--

CREATE TABLE Fact_Complaints(
    CMPLNT_NUM           INT,
    X_COORD_CD           INT,
    Y_COORD_CD           INT,
    Latitude             INT,
    Longitude            INT,
    TRANSIT_DISTRICT     CHAR(50),
    RPT_DT               DATETIME,
    STATION_NAME         CHAR(50),
    PREM_TYP_DESC        CHAR(50),
    PARKS_NM             CHAR(50),
    PATROL_BORO          CHAR(50),
    LOC_OF_OCCUR_DESC    CHAR(50),
    HADEVELOPT           CHAR(50),
    HOUSING_PSA          INT,
    CRM_ATPT_CPTD_CD     CHAR(50),
    CMPLNT_FR_DT         DATETIME,
    CMPLNT_FR_TM         DATETIME,
    CMPLNT_TO_DT         DATETIME,
    CMPLNT_TO_TM         DATETIME,
    ADDR_PCT_CD          INT,
    KY_SK                INT         NOT NULL,
    PD_SK                INT         NOT NULL,
    Sex_SK               INT         NOT NULL,
    Race_SK              INT         NOT NULL,
    Age_SK               INT         NOT NULL,
    Boro_SK              INT         NOT NULL,
    Jurisdiction_SK      INT         NOT NULL
)ENGINE=MYISAM
;



-- 
-- TABLE: Fact_CriminalCourt_Summon 
--

CREATE TABLE Fact_CriminalCourt_Summon(
    SUMMONS_KEY              INT,
    X_COORD_CD               INT,
    Y_COORD_CD               INT,
    Latitude                 INT,
    Longitude                INT,
    PRECINCT_OF_OCCUR        CHAR(50),
    SUMMONS_CATEGORY_TYPE    CHAR(50),
    OFFENSE_DESCRIPTION      CHAR(50),
    SUMMONS_DATE             DATETIME,
    LAWSECTION_SK            INT         NOT NULL,
    Age_SK                   INT         NOT NULL,
    Race_SK                  INT         NOT NULL,
    Sex_SK                   INT         NOT NULL,
    Jurisdiction_SK          INT         NOT NULL,
    Boro_SK                  INT         NOT NULL
)ENGINE=MYISAM
;



-- 
-- TABLE: Fact_Shooting_Incidents 
--

CREATE TABLE Fact_Shooting_Incidents(
    SUMMONS_KEY                INT,
    X_COORD_CD                 INT,
    Y_COORD_CD                 INT,
    OCCUR_DATE                 DATETIME,
    OCCUR_TIME                 TIME,
    PRECINCT                   CHAR(50),
    LOCATION_DESC              CHAR(50),
    STATISTICAL_MURDER_FLAG    CHAR(50),
    Jurisdiction_SK            INT         NOT NULL,
    Boro_SK                    INT         NOT NULL,
    Sex_SK                     INT         NOT NULL,
    Race_SK                    INT         NOT NULL,
    Age_SK                     INT         NOT NULL
)ENGINE=MYISAM
;



-- 
-- TABLE: Fact_Complaints 
--

ALTER TABLE Fact_Complaints ADD CONSTRAINT RefDim_KY_Code1 
    FOREIGN KEY (KY_SK)
    REFERENCES Dim_KY_Code(KY_SK)
;

ALTER TABLE Fact_Complaints ADD CONSTRAINT RefDim_PD_Code2 
    FOREIGN KEY (PD_SK)
    REFERENCES Dim_PD_Code(PD_SK)
;

ALTER TABLE Fact_Complaints ADD CONSTRAINT RefDim_Sex3 
    FOREIGN KEY (Sex_SK)
    REFERENCES Dim_Sex(Sex_SK)
;

ALTER TABLE Fact_Complaints ADD CONSTRAINT RefDim_Race4 
    FOREIGN KEY (Race_SK)
    REFERENCES Dim_Race(Race_SK)
;

ALTER TABLE Fact_Complaints ADD CONSTRAINT RefDim_Age_Group5 
    FOREIGN KEY (Age_SK)
    REFERENCES Dim_Age_Group(Age_SK)
;

ALTER TABLE Fact_Complaints ADD CONSTRAINT RefDim_Borough6 
    FOREIGN KEY (Boro_SK)
    REFERENCES Dim_Borough(Boro_SK)
;

ALTER TABLE Fact_Complaints ADD CONSTRAINT RefDim_Jurisdiction7 
    FOREIGN KEY (Jurisdiction_SK)
    REFERENCES Dim_Jurisdiction(Jurisdiction_SK)
;


-- 
-- TABLE: Fact_CriminalCourt_Summon 
--

ALTER TABLE Fact_CriminalCourt_Summon ADD CONSTRAINT RefDim_LawSection8 
    FOREIGN KEY (LAWSECTION_SK)
    REFERENCES Dim_LawSection(LAWSECTION_SK)
;

ALTER TABLE Fact_CriminalCourt_Summon ADD CONSTRAINT RefDim_Age_Group9 
    FOREIGN KEY (Age_SK)
    REFERENCES Dim_Age_Group(Age_SK)
;

ALTER TABLE Fact_CriminalCourt_Summon ADD CONSTRAINT RefDim_Race10 
    FOREIGN KEY (Race_SK)
    REFERENCES Dim_Race(Race_SK)
;

ALTER TABLE Fact_CriminalCourt_Summon ADD CONSTRAINT RefDim_Sex11 
    FOREIGN KEY (Sex_SK)
    REFERENCES Dim_Sex(Sex_SK)
;

ALTER TABLE Fact_CriminalCourt_Summon ADD CONSTRAINT RefDim_Jurisdiction12 
    FOREIGN KEY (Jurisdiction_SK)
    REFERENCES Dim_Jurisdiction(Jurisdiction_SK)
;

ALTER TABLE Fact_CriminalCourt_Summon ADD CONSTRAINT RefDim_Borough13 
    FOREIGN KEY (Boro_SK)
    REFERENCES Dim_Borough(Boro_SK)
;


-- 
-- TABLE: Fact_Shooting_Incidents 
--

ALTER TABLE Fact_Shooting_Incidents ADD CONSTRAINT RefDim_Jurisdiction14 
    FOREIGN KEY (Jurisdiction_SK)
    REFERENCES Dim_Jurisdiction(Jurisdiction_SK)
;

ALTER TABLE Fact_Shooting_Incidents ADD CONSTRAINT RefDim_Borough15 
    FOREIGN KEY (Boro_SK)
    REFERENCES Dim_Borough(Boro_SK)
;

ALTER TABLE Fact_Shooting_Incidents ADD CONSTRAINT RefDim_Sex16 
    FOREIGN KEY (Sex_SK)
    REFERENCES Dim_Sex(Sex_SK)
;

ALTER TABLE Fact_Shooting_Incidents ADD CONSTRAINT RefDim_Race17 
    FOREIGN KEY (Race_SK)
    REFERENCES Dim_Race(Race_SK)
;

ALTER TABLE Fact_Shooting_Incidents ADD CONSTRAINT RefDim_Age_Group18 
    FOREIGN KEY (Age_SK)
    REFERENCES Dim_Age_Group(Age_SK)
;


