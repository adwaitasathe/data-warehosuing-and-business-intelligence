--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      AdventureWorksPurchasingDW Final.DM1
--
-- Date Created : Sunday, February 23, 2020 14:48:31
-- Target DBMS : MySQL 5.x
--

-- 
-- TABLE: Dim_RejectCodes 
--

CREATE TABLE Dim_RejectCodes(

)ENGINE=MYISAM
;



-- 
-- TABLE: Dim_SORSystem 
--

CREATE TABLE Dim_SORSystem(
    SOR_SK            INT            AUTO_INCREMENT,
    SOR_Name          VARCHAR(50),
    SOR_Module        VARCHAR(50),
    SOR_Table         VARCHAR(50),
    SOR_Attributes    VARCHAR(50),
    PRIMARY KEY (SOR_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimCalendar 
--

CREATE TABLE DimCalendar(
    DateKey                 INT                     NOT NULL,
    FullDateAlternateKey    DATE                    NOT NULL,
    DayNumberOfWeek         TINYINT                 NOT NULL,
    EnglishDayNameOfWeek    NATIONAL VARCHAR(10)    NOT NULL,
    DayNumberOfMonth        TINYINT                 NOT NULL,
    DayNumberOfYear         SMALLINT                NOT NULL,
    WeekNumberOfYear        TINYINT                 NOT NULL,
    EnglishMonthName        NATIONAL VARCHAR(10)    NOT NULL,
    MonthNumberOfYear       TINYINT                 NOT NULL,
    CalendarQuarter         TINYINT                 NOT NULL,
    CalendarYear            SMALLINT                NOT NULL,
    CalendarSemester        TINYINT                 NOT NULL,
    FiscalQuarter           TINYINT                 NOT NULL,
    FiscalYear              SMALLINT                NOT NULL,
    FiscalSemester          TINYINT                 NOT NULL,
    PRIMARY KEY (DateKey)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimEmployee 
--

CREATE TABLE DimEmployee(
    EmployeeKey_SK                    INT                      AUTO_INCREMENT,
    SalesTerritoryKey                 INT,
    VendorKey_SK                      INT                      NOT NULL,
    EmployeeNationalIDAlternateKey    NATIONAL VARCHAR(15),
    PersonType                        NATIONAL CHAR(2)         NOT NULL,
    FirstName                         NATIONAL VARCHAR(50)     NOT NULL,
    LastName                          NATIONAL VARCHAR(50)     NOT NULL,
    MiddleName                        NATIONAL VARCHAR(50),
    NameStyle                         BIT(1)                   NOT NULL,
    Title                             NATIONAL VARCHAR(50),
    HireDate                          DATE,
    BirthDate                         DATE,
    LoginID                           NATIONAL VARCHAR(256),
    EmailAddress                      NATIONAL VARCHAR(50),
    Phone                             NATIONAL VARCHAR(25),
    MaritalStatus                     NATIONAL CHAR(1),
    SalariedFlag                      BIT(1),
    Gender                            NATIONAL CHAR(1),
    PRIMARY KEY (EmployeeKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimGeography 
--

CREATE TABLE DimGeography(
    GeographyKey                INT                     AUTO_INCREMENT,
    SalesTerritoryKey           INT                     NOT NULL,
    City                        NATIONAL VARCHAR(30),
    StateProvinceCode           NATIONAL VARCHAR(3),
    StateProvinceName           NATIONAL VARCHAR(50),
    CountryRegionCode           NATIONAL VARCHAR(3),
    EnglishCountryRegionName    NATIONAL VARCHAR(50),
    PostalCode                  NATIONAL VARCHAR(15),
    PRIMARY KEY (GeographyKey)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimListPriceHistory 
--

CREATE TABLE DimListPriceHistory(
    ListPriceHistory_SK    INT               AUTO_INCREMENT,
    ProductKey_SK          INT               NOT NULL,
    Effective_Date         DATETIME,
    Ineffective_Date       DATETIME,
    Current_ListPrice      DECIMAL(10, 0),
    Status                 BIT(1),
    PRIMARY KEY (ListPriceHistory_SK, ProductKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimLocation 
--

CREATE TABLE DimLocation(
    LocationKey_NK    INT                     NOT NULL,
    LocationName      NATIONAL VARCHAR(50),
    PRIMARY KEY (LocationKey_NK)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimPayHistory 
--

CREATE TABLE DimPayHistory(
    PayHistory_SK       INT               AUTO_INCREMENT,
    EmployeeKey_SK      INT               NOT NULL,
    EffectiveDate       DATETIME,
    IneffectiveDate     DATETIME,
    CurrentIndiactor    BIT(1),
    PayRate             DECIMAL(10, 0),
    PayFrequency        BIT(1),
    PRIMARY KEY (PayHistory_SK, EmployeeKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimProduct 
--

CREATE TABLE DimProduct(
    ProductKey_SK             INT                     AUTO_INCREMENT,
    ProductKey_NK             INT                     NOT NULL,
    ProductName               NATIONAL VARCHAR(50),
    ProductNumber             NATIONAL VARCHAR(25),
    FinishedGoodFlag          BIT(1),
    Colour                    NATIONAL VARCHAR(15),
    SafetyStockLevel          SMALLINT                NOT NULL,
    ReorderPoint              SMALLINT                NOT NULL,
    Size                      NATIONAL VARCHAR(5),
    SizeUnitMeasureCode       NATIONAL CHAR(3),
    WeightUnitMeasureCode     NATIONAL CHAR(3),
    Weight                    DECIMAL(8, 2),
    DaysToManufacture         INT,
    ProductLine               NATIONAL CHAR(2),
    Class                     NATIONAL CHAR(2),
    Style                     NATIONAL CHAR(2),
    ProductSubCategoryName    NATIONAL VARCHAR(50),
    ProductCategoryName       NATIONAL VARCHAR(50),
    ProductModelName          NATIONAL VARCHAR(50),
    PRIMARY KEY (ProductKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimSalesTerritory 
--

CREATE TABLE DimSalesTerritory(
    SalesTerritoryKey             INT                     AUTO_INCREMENT,
    SalesTerritoryAlternateKey    INT,
    SalesTerritoryRegion          NATIONAL VARCHAR(50)    NOT NULL,
    SalesTerritoryCountry         NATIONAL VARCHAR(50)    NOT NULL,
    SalesTerritoryGroup           NATIONAL VARCHAR(50),
    PRIMARY KEY (SalesTerritoryKey)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimScrapReason 
--

CREATE TABLE DimScrapReason(
    ScrapReasonId_NK    INT            NOT NULL,
    Name                VARCHAR(10),
    PRIMARY KEY (ScrapReasonId_NK)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimStandardCostHistory 
--

CREATE TABLE DimStandardCostHistory(
    StandardCostHistory_SK    INT               AUTO_INCREMENT,
    ProductKey_SK             INT               NOT NULL,
    Effective_Date            DATETIME,
    Ineffective_Date          DATETIME,
    Current_StandardCost      DECIMAL(10, 0)    NOT NULL,
    Status                    BIT(1),
    PRIMARY KEY (StandardCostHistory_SK, ProductKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimVendor 
--

CREATE TABLE DimVendor(
    VendorKey_SK               INT                      AUTO_INCREMENT,
    VendorKey_NK               INT,
    AccountNumber              NATIONAL VARCHAR(15),
    VendorName                 NATIONAL VARCHAR(50),
    CreditRating               TINYINT,
    PreferredVendorStatus      BIT(1),
    ActiveFlag                 BIT(1),
    PurchasingWebServiceURL    NATIONAL VARCHAR(120),
    SalesTerritoryKey          INT                      NOT NULL,
    PRIMARY KEY (VendorKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: FactProductInventory 
--

CREATE TABLE FactProductInventory(
    ProductKey_SK     INT                     NOT NULL,
    Quantity          SMALLINT                NOT NULL,
    Bin               TINYINT,
    Shelf             NATIONAL VARCHAR(12),
    LocationKey_NK    INT                     NOT NULL,
    PRIMARY KEY (ProductKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: FactProductVendor 
--

CREATE TABLE FactProductVendor(
    VendorKey_SK          INT               NOT NULL,
    ProductKey_SK         INT               NOT NULL,
    AverageLeadTime       INT,
    StandardPrice         DECIMAL(10, 0),
    LastReceiptCost       DECIMAL(10, 0),
    LastReceiptDate       DATETIME,
    LastReceiptDateKey    INT,
    PRIMARY KEY (VendorKey_SK, ProductKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: FactPurchase 
--

CREATE TABLE FactPurchase(
    VendorKey_SK           INT                     NOT NULL,
    ProductKey_SK          INT                     NOT NULL,
    EmployeeKey_SK         INT                     NOT NULL,
    ShippingCompanyName    NATIONAL VARCHAR(50),
    ShipBase               DECIMAL(10, 0),
    ShipRate               DECIMAL(10, 0),
    PurchaseOrderID        INT,
    ProductKey             INT,
    EmployeeKey            CHAR(10),
    PurchaseOrderLineID    INT,
    DueDate                DATETIME,
    OrderDate              DATETIME,
    ShipDate               DATETIME,
    OrderQty               SMALLINT,
    UnitPrice              DECIMAL(10, 0),
    ReceivedQty            DECIMAL(8, 2),
    RejectedQty            DECIMAL(8, 2),
    StockedQty             DECIMAL(9, 2),
    RevisionNumber         TINYINT,
    SubTotal               DECIMAL(10, 0),
    TaxAm                  DECIMAL(10, 0),
    Freight                DECIMAL(10, 0),
    ProductStandardCost    DECIMAL(10, 0),
    TotalDue               DECIMAL(10, 0),
    ProductTotalCost       DECIMAL(10, 0),
    DueDateKey             INT                     NOT NULL,
    ShipDateKey            INT                     NOT NULL,
    OrderDateKey           INT                     NOT NULL,
    SalesTerritoryKey      INT                     NOT NULL,
    PRIMARY KEY (VendorKey_SK, ProductKey_SK, EmployeeKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: FactPurchase_Rejects 
--

CREATE TABLE FactPurchase_Rejects(
    ProductKey_SK          INT                     NOT NULL,
    EmployeeKey_SK         INT                     NOT NULL,
    VendorKey_SK           INT                     NOT NULL,
    ShippingCompanyName    NATIONAL VARCHAR(50),
    ShipBase               DECIMAL(10, 0),
    ShipRate               DECIMAL(10, 0),
    PurchaseOrderID        INT,
    ProductKey             INT,
    EmployeeKey            CHAR(10),
    PurchaseOrderLineID    INT,
    DueDate                DATETIME,
    OrderDate              DATETIME,
    ShipDate               DATETIME,
    OrderQty               SMALLINT,
    UnitPrice              DECIMAL(10, 0),
    ReceivedQty            DECIMAL(8, 2),
    RejectedQty            DECIMAL(8, 2),
    StockedQty             DECIMAL(9, 2),
    RevisionNumber         TINYINT,
    SubTotal               DECIMAL(10, 0),
    TaxAm                  DECIMAL(10, 0),
    Freight                DECIMAL(10, 0),
    ProductStandardCost    DECIMAL(10, 0),
    TotalDue               DECIMAL(10, 0),
    ProductTotalCost       DECIMAL(10, 0),
    DueDateKey             INT                     NOT NULL,
    ShipDateKey            INT                     NOT NULL,
    OrderDateKey           INT                     NOT NULL,
    SalesTerritoryKey      INT                     NOT NULL,
    PRIMARY KEY (ProductKey_SK, EmployeeKey_SK, VendorKey_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: FactWorkOrder 
--

CREATE TABLE FactWorkOrder(
    FactWorkOrder_SK    INT         AUTO_INCREMENT,
    OrderQty            INT,
    StockedQty          INT,
    ScrappedQty         INT,
    StartDate           DATETIME,
    EndDate             DATETIME,
    DueDate             DATETIME,
    ProductKey_SK       INT         NOT NULL,
    ScrapReasonId_NK    INT         NOT NULL,
    PRIMARY KEY (FactWorkOrder_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: FactWorkOrder_Rejects 
--

CREATE TABLE FactWorkOrder_Rejects(
    FactWorkOrder_Reject_SK    INT         AUTO_INCREMENT,
    OrderQty                   INT,
    StockedQty                 INT,
    ScrappedQty                INT,
    StartDate                  DATETIME,
    EndDate                    DATETIME,
    DueDate                    DATETIME,
    ProductKey_SK              INT         NOT NULL,
    PRIMARY KEY (FactWorkOrder_Reject_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: FactWorkOrderRouting 
--

CREATE TABLE FactWorkOrderRouting(
    FactOrderRouting_SK    INT               AUTO_INCREMENT,
    ScheduledStartDate     DATETIME,
    ActualStartDate        DATETIME,
    ActualEndDate          DATETIME,
    ActualResourceHours    DECIMAL(9, 4),
    PlannedCost            DECIMAL(10, 0),
    ActualCost             DECIMAL(10, 0),
    ProductKey_SK          INT               NOT NULL,
    FactWorkOrder_SK       INT               NOT NULL,
    LocationKey_NK         INT               NOT NULL,
    PRIMARY KEY (FactOrderRouting_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: FactWorkOrderRouting_Rejects 
--

CREATE TABLE FactWorkOrderRouting_Rejects(
    FactOrderRouting_Reject_SK    INT               AUTO_INCREMENT,
    ScheduledStartDate            DATETIME,
    ActualStartDate               DATETIME,
    ActualEndDate                 DATETIME,
    ActualResourceHours           DECIMAL(9, 4),
    PlannedCost                   DECIMAL(10, 0),
    ActualCost                    DECIMAL(10, 0),
    ProductKey_SK                 INT               NOT NULL,
    FactWorkOrder_SK              INT               NOT NULL,
    LocationKey_NK                INT               NOT NULL,
    PRIMARY KEY (FactOrderRouting_Reject_SK)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimEmployee 
--

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimVendor31 
    FOREIGN KEY (VendorKey_SK)
    REFERENCES DimVendor(VendorKey_SK)
;


-- 
-- TABLE: DimGeography 
--

ALTER TABLE DimGeography ADD CONSTRAINT RefDimSalesTerritory48 
    FOREIGN KEY (SalesTerritoryKey)
    REFERENCES DimSalesTerritory(SalesTerritoryKey)
;


-- 
-- TABLE: DimListPriceHistory 
--

ALTER TABLE DimListPriceHistory ADD CONSTRAINT RefDimProduct30 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;


-- 
-- TABLE: DimPayHistory 
--

ALTER TABLE DimPayHistory ADD CONSTRAINT RefDimEmployee32 
    FOREIGN KEY (EmployeeKey_SK)
    REFERENCES DimEmployee(EmployeeKey_SK)
;


-- 
-- TABLE: DimStandardCostHistory 
--

ALTER TABLE DimStandardCostHistory ADD CONSTRAINT RefDimProduct8 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;


-- 
-- TABLE: DimVendor 
--

ALTER TABLE DimVendor ADD CONSTRAINT RefDimSalesTerritory47 
    FOREIGN KEY (SalesTerritoryKey)
    REFERENCES DimSalesTerritory(SalesTerritoryKey)
;


-- 
-- TABLE: FactProductInventory 
--

ALTER TABLE FactProductInventory ADD CONSTRAINT RefDimProduct29 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;

ALTER TABLE FactProductInventory ADD CONSTRAINT RefDimLocation57 
    FOREIGN KEY (LocationKey_NK)
    REFERENCES DimLocation(LocationKey_NK)
;


-- 
-- TABLE: FactProductVendor 
--

ALTER TABLE FactProductVendor ADD CONSTRAINT RefDimVendor10 
    FOREIGN KEY (VendorKey_SK)
    REFERENCES DimVendor(VendorKey_SK)
;

ALTER TABLE FactProductVendor ADD CONSTRAINT RefDimProduct12 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;


-- 
-- TABLE: FactPurchase 
--

ALTER TABLE FactPurchase ADD CONSTRAINT RefDimSalesTerritory49 
    FOREIGN KEY (SalesTerritoryKey)
    REFERENCES DimSalesTerritory(SalesTerritoryKey)
;

ALTER TABLE FactPurchase ADD CONSTRAINT RefDimVendor9 
    FOREIGN KEY (VendorKey_SK)
    REFERENCES DimVendor(VendorKey_SK)
;

ALTER TABLE FactPurchase ADD CONSTRAINT RefDimProduct13 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;

ALTER TABLE FactPurchase ADD CONSTRAINT RefDimEmployee16 
    FOREIGN KEY (EmployeeKey_SK)
    REFERENCES DimEmployee(EmployeeKey_SK)
;

ALTER TABLE FactPurchase ADD CONSTRAINT RefDimCalendar25 
    FOREIGN KEY (DueDateKey)
    REFERENCES DimCalendar(DateKey)
;

ALTER TABLE FactPurchase ADD CONSTRAINT RefDimCalendar26 
    FOREIGN KEY (OrderDateKey)
    REFERENCES DimCalendar(DateKey)
;

ALTER TABLE FactPurchase ADD CONSTRAINT RefDimCalendar27 
    FOREIGN KEY (ShipDateKey)
    REFERENCES DimCalendar(DateKey)
;


-- 
-- TABLE: FactPurchase_Rejects 
--

ALTER TABLE FactPurchase_Rejects ADD CONSTRAINT RefDimCalendar73 
    FOREIGN KEY (DueDateKey)
    REFERENCES DimCalendar(DateKey)
;

ALTER TABLE FactPurchase_Rejects ADD CONSTRAINT RefDimCalendar74 
    FOREIGN KEY (ShipDateKey)
    REFERENCES DimCalendar(DateKey)
;

ALTER TABLE FactPurchase_Rejects ADD CONSTRAINT RefDimCalendar75 
    FOREIGN KEY (OrderDateKey)
    REFERENCES DimCalendar(DateKey)
;

ALTER TABLE FactPurchase_Rejects ADD CONSTRAINT RefDimSalesTerritory76 
    FOREIGN KEY (SalesTerritoryKey)
    REFERENCES DimSalesTerritory(SalesTerritoryKey)
;

ALTER TABLE FactPurchase_Rejects ADD CONSTRAINT RefDimVendor78 
    FOREIGN KEY (VendorKey_SK)
    REFERENCES DimVendor(VendorKey_SK)
;

ALTER TABLE FactPurchase_Rejects ADD CONSTRAINT RefDimProduct79 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;

ALTER TABLE FactPurchase_Rejects ADD CONSTRAINT RefDimEmployee80 
    FOREIGN KEY (EmployeeKey_SK)
    REFERENCES DimEmployee(EmployeeKey_SK)
;


-- 
-- TABLE: FactWorkOrder 
--

ALTER TABLE FactWorkOrder ADD CONSTRAINT RefDimProduct58 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;

ALTER TABLE FactWorkOrder ADD CONSTRAINT RefDimScrapReason68 
    FOREIGN KEY (ScrapReasonId_NK)
    REFERENCES DimScrapReason(ScrapReasonId_NK)
;


-- 
-- TABLE: FactWorkOrder_Rejects 
--

ALTER TABLE FactWorkOrder_Rejects ADD CONSTRAINT RefDimProduct60 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;


-- 
-- TABLE: FactWorkOrderRouting 
--

ALTER TABLE FactWorkOrderRouting ADD CONSTRAINT RefDimProduct61 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;

ALTER TABLE FactWorkOrderRouting ADD CONSTRAINT RefFactWorkOrder62 
    FOREIGN KEY (FactWorkOrder_SK)
    REFERENCES FactWorkOrder(FactWorkOrder_SK)
;

ALTER TABLE FactWorkOrderRouting ADD CONSTRAINT RefDimLocation63 
    FOREIGN KEY (LocationKey_NK)
    REFERENCES DimLocation(LocationKey_NK)
;


-- 
-- TABLE: FactWorkOrderRouting_Rejects 
--

ALTER TABLE FactWorkOrderRouting_Rejects ADD CONSTRAINT RefFactWorkOrder64 
    FOREIGN KEY (FactWorkOrder_SK)
    REFERENCES FactWorkOrder(FactWorkOrder_SK)
;

ALTER TABLE FactWorkOrderRouting_Rejects ADD CONSTRAINT RefDimLocation65 
    FOREIGN KEY (LocationKey_NK)
    REFERENCES DimLocation(LocationKey_NK)
;

ALTER TABLE FactWorkOrderRouting_Rejects ADD CONSTRAINT RefDimProduct66 
    FOREIGN KEY (ProductKey_SK)
    REFERENCES DimProduct(ProductKey_SK)
;


